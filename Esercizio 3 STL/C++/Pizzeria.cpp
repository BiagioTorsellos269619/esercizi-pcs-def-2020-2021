#include "Pizzeria.h"
#include <string>
#include <list>
namespace PizzeriaLibrary {

void Pizzeria::AddIngredient(const string &name, const string &description, const int &price) {

    Ingredient ingredient;
    ingredient.Name = name;
    ingredient.Description = description;
    ingredient.Price = price;

    if(ingredientsPizzeria.count(name) == 0) //non c'è l'ingrediente
    {
        ingredientsPizzeria.insert(pair<string, Ingredient>(name, ingredient));
    }
    else
    {
        throw runtime_error("Ingredient already inserted");
    }

}

const Ingredient &Pizzeria::FindIngredient(const string &name) const {

    if(ingredientsPizzeria.count(name) == 0)
    {
        //non c'è l'ingrediente
        throw runtime_error("Ingredient not found");
    }
    return ingredientsPizzeria.find(name)->second;
}

void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients) {

    if((menuPizze.count(name) == 0)) //ovvero, non trovo nessun elemento con quella chiave.
    {
        vector<Ingredient> ingredientiPizza;
        for (unsigned int i = 0; i < ingredients.size(); i++)
        {
            ingredientiPizza.push_back(Ingredient());
            ingredientiPizza[i].Description = ingredientsPizzeria.find(ingredients[i])->second.Description;
            ingredientiPizza[i].Name = ingredientsPizzeria.find(ingredients[i])->second.Name;
            ingredientiPizza[i].Price = ingredientsPizzeria.find(ingredients[i])->second.Price;
        }

        Pizza pizza = Pizza(name,ingredientiPizza);
        menuPizze.insert(pair<string, Pizza>(name, pizza));
    }
    else
    {
        throw runtime_error("Pizza already inserted");
    }
}

const Pizza &Pizzeria::FindPizza(const string &name) const {

    if(menuPizze.count(name) == 0)
    {
        //non ho trovato la pizza
         throw runtime_error("Pizza not found");
    }

    return menuPizze.find(name)->second;
}

int Pizzeria::CreateOrder(const vector<string> &pizzas) {
    if(pizzas == vector<string>())
    {
        throw runtime_error("Empty order");
    }

    Order newOrder;
    newOrder.numOrdine = 1000 + totalOrders.size();

    ///inserisco le pizze nell'ordine
    for(unsigned int i = 0; i < pizzas.size(); i++)
    {
        Pizza pizza = menuPizze.find(pizzas[i])->second;
        newOrder.pizzasOrder.push_back(pizza);
        newOrder.totalPriceOrder = newOrder.totalPriceOrder + pizza.ComputePrice();
     }

    totalOrders.insert(pair<int, Order>(newOrder.numOrdine, newOrder));

    return newOrder.numOrdine;
}

const Order &Pizzeria::FindOrder(const int &numOrder) const {

    if(totalOrders.count(numOrder) == 0)
    {
       throw runtime_error("Order not found");
    }
    return totalOrders.find(numOrder)->second;
}

string Pizzeria::GetReceipt(const int &numOrder) const {
    Order ordineTrovato = FindOrder(numOrder);
    string receipt;
    for(unsigned int i = 0; i < ordineTrovato.pizzasOrder.size(); i++)
        {
            //"- " + name + ", " + price + " euro" + "\n"
            //"  TOTAL: " + total + " euro" + "\n"
            string costoPizza = to_string(ordineTrovato.pizzasOrder[i].ComputePrice());
            receipt = receipt + "- " + ordineTrovato.pizzasOrder[i].Name + ", " + costoPizza + " euro" + "\n";
         }
         string costoTotale = to_string(ordineTrovato.totalPriceOrder);
         receipt = receipt + "  TOTAL: " + costoTotale + " euro" + "\n";

     return receipt;
}

string Pizzeria::ListIngredients() const {

    map<string, Ingredient>::iterator it;
    string listIngredients;
    for (auto it = ingredientsPizzeria.begin(); it != ingredientsPizzeria.end(); ++it)
    {
        string prezzo = to_string(it->second.Price);
        listIngredients = listIngredients + it->second.Name + " - " + "'" + it->second.Description + "'" +  ": ";
        listIngredients = listIngredients + prezzo + " euro" + "\n";
    }
    return listIngredients;
}

string Pizzeria::Menu() const {
    string menuStampato;
    int price = 0;

    for (auto it = menuPizze.begin(); it != menuPizze.end(); ++it)
     {
         menuStampato = menuStampato + it->first + " (";
         string numIngredients = to_string(it->second.ingredientsPizza.size());
         menuStampato = menuStampato + numIngredients + " ingredients): ";
         for(unsigned int i = 0; i < it->second.ingredientsPizza.size(); i++)
         {
            price = price + ingredientsPizzeria.find(it->second.ingredientsPizza[i].Name)->second.Price;
         }
         string priceString = to_string(price);
         menuStampato = menuStampato + priceString + " euro";
         menuStampato = menuStampato + "\n";
         price = 0;

}
    return menuStampato;
}

const Pizza &Order::GetPizza(const int &position) const {
    if(position >= int(pizzasOrder.size()))
    {
        throw runtime_error("Position passed is wrong");
    }
    return pizzasOrder[position - 1];
}

void Pizza::AddIngredient(const Ingredient &ingredient) {
    ingredientsPizza.push_back(Ingredient());
    ingredientsPizza[ingredientsPizza.size() - 1].Description = ingredient.Description;
    ingredientsPizza[ingredientsPizza.size() - 1].Price = ingredient.Price;
    ingredientsPizza[ingredientsPizza.size() - 1].Name = ingredient.Name;
}

int Pizza::ComputePrice() const {
    int prezzoTotale = 0;
    for(unsigned int i = 0; i < ingredientsPizza.size(); i++)
    {
        prezzoTotale = prezzoTotale + ingredientsPizza[i].Price;
    }
    return prezzoTotale;
}

}
