class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description

class Pizza:
    def __init__(self, name: str, ingredientiPizza : [], price):
        self.Name = name
        self.__IngredientsPizza = ingredientiPizza  #vettore di nomi
        self.Price = price

    def addIngredient(self, ingredient: Ingredient):
        self.__IngredientsPizza.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.__IngredientsPizza)

    def computePrice(self) -> int:
        return self.Price


class Order:
    def __init__(self, numOrder : int, totalPriceOrder : int, pizzasOrder : []):
        self.__NumOrder = numOrder
        self.__TotalPriceOrder = totalPriceOrder
        self.__pizzasOrder = pizzasOrder
    def getPizza(self, position: int) -> Pizza:
        if (position >= int(len(self.__pizzasOrder))):
          raise Exception("Position passed is wrong")
        return self.__pizzasOrder[position]

    def initializeOrder(self, numPizzas: int):
        for i in range(0, numPizzas - 1):
          self.__pizzasOrder.append(Pizza(""))

    def addPizza(self, pizza: Pizza):
        self.__pizzasOrder.append(pizza)

    def numPizzas(self) -> int:
        return len(self.__pizzasOrder)

    def computeTotal(self) -> int:
        return self.__TotalPriceOrder


class Pizzeria:
    def __init__(self):
        self.menuPizze = {}
        self._totalOrders = {}
        self.__ingredientsPizzeria = {}
        self.__nomiOrdinatiIng = [] #variabile per stampare in ordine alfabetico gli ingredienti della pizzeria
        self.__nomiPizze = []
        self.__prezziIngredienti = []
        #self.__prezziPizze = []

    def addIngredient(self, name: str, description: str, price: int):

        if name not in self.__ingredientsPizzeria:
            self.__ingredientsPizzeria[name] = Ingredient(name, price, description)
            self.__nomiOrdinatiIng.append(name)
            self.__prezziIngredienti.append(price)
            #scrivo in ordine alfabetico gli ingredienti
            self.__nomiOrdinatiIng.sort()
        else:
            raise Exception("Ingredient already inserted")

    def findIngredient(self, name: str) -> Ingredient:
        if name not in self.__ingredientsPizzeria:
           raise Exception("Ingredient not found")
        else:
           return self.__ingredientsPizzeria[name]

    def addPizza(self, name: str, ingredients: []):
        if name not in self.menuPizze:
            prezzoTot : int = 0
            for i in range (0, len(ingredients)):
               prezzoTot = prezzoTot + self.__prezziIngredienti[i] 

            self.menuPizze[name] = Pizza(name, ingredients, prezzoTot)
            self.__nomiPizze.append(name)
        else:
            raise Exception("Pizza already inserted")

    def findPizza(self, name: str) -> Pizza:
        if name not in self.menuPizze:
            raise Exception("Pizza not found")
        else:
            return self.menuPizze[name]

    def createOrder(self, pizzas: []) -> int:
        if(len(pizzas) == 0):
            raise Exception("Empty order")
        else:
            pizzeria : Pizzeria
            prezzoTotal : int = 0
            listaPizze = []
            numeroOrdine = 1000 + len(self._totalOrders)
            for i in range(0, len(pizzas)):
                pizza : Pizza = self.findPizza(pizzas[i])
                listaPizze.append(pizza)
                prezzoTotal = prezzoTotal + pizza.Price

            self._totalOrders[numeroOrdine] = Order(numeroOrdine, prezzoTotal, listaPizze)

        return numeroOrdine

    def findOrder(self, numOrder: int) -> Order:

        listaChiavi = []
        trovato : int = 0
        listaChiavi = self._totalOrders.keys()
        for i in range(0, len(listaChiavi)):
            if numOrder in listaChiavi:
                trovato = 1
        if(trovato == 0):
            raise Exception("Order not found")
        return self._totalOrders[numOrder]

    def getReceipt(self, numOrder: int) -> str:
        ordineTrovato : Order = self.findOrder(numOrder)
        receipt : string = ""
        costoPizza : string = ""
        costoTotale : string = ""
        nomePizza : string = ""
        for i in range (0, ordineTrovato.numPizzas()):
            pizza : Pizza = ordineTrovato.getPizza(i)
            costoPizza = str(pizza.Price)
            nomePizza = pizza.Name
            receipt = receipt + "- " + nomePizza + ", " + costoPizza + " euro" + "\n"

        costoTotale = str(ordineTrovato.computeTotal())
        receipt = receipt + "  TOTAL: " + costoTotale + " euro" + "\n"
        return receipt


    def listIngredients(self) -> str:
        listaIngredienti = ""
        for i in range(0, len(self.__nomiOrdinatiIng)):
            name = self.__nomiOrdinatiIng[i]
            prezzo = str(self.__ingredientsPizzeria[name].Price)
            listaIngredienti = listaIngredienti + name + " - " + "'" + self.__ingredientsPizzeria[name].Description + "'" + ": "
            listaIngredienti = listaIngredienti + prezzo + " euro" + "\n"

        return listaIngredienti

    def menu(self) -> str:
        menu_stampato : string = ""
        num_ingredients : string = ""

        for i in range(0, len(self.__nomiPizze)):
            menu_stampato = menu_stampato + self.__nomiPizze[i] + " ("
            pizza : Pizza = self.findPizza(self.__nomiPizze[i])
            num_ingredients = str(pizza.numIngredients())
            menu_stampato = menu_stampato + num_ingredients + " ingredients): "
            price : int = pizza.Price
            priceString: string = str(price)
            menu_stampato = menu_stampato + priceString + " euro"
            menu_stampato = menu_stampato + "\n"

        return menu_stampato

