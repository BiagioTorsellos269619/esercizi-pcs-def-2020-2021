# include "viaMichelin.h"

#include <iostream>
#include <fstream>
#include <sstream>

namespace ViaMichelinLibrary {
int RoutePlanner::BusAverageSpeed = 50;

void BusStation::Load() {

    _busesNumber = 0;
    _buses.clear();
    ifstream file;
    file.open(_busFilePath.c_str());
    if(file.fail())
    {
        throw runtime_error("Something goes wrong");
    }

    try {
      string line;
      getline(file, line); //salto la prima riga.
      getline(file, line); //salvo il numero di bus;
      _busesNumber = stoi(line); //converto il numero di bus da stringa ad intero

      /* istringstream convertN;
      convertN.str(line);
      convertN >> _numberBus;*/
      ///salto due righe di commento
      getline(file, line);
      getline(file, line);

      //riempio il vettore
      _buses.reserve(_busesNumber);
      for (int b = 0; b < _busesNumber; b++)
      {
        _buses.push_back(Bus());
        _buses[b].Id = b + 1;
        //riempio il vettore leggendo il costo del carburante da file.
        getline(file, line);
        vector<char> vettoreAppoggio;
        vettoreAppoggio.reserve(line.size() + 1);
        string numeroBus;
        for(unsigned int i = 2; i < line.size(); i++)
        {
             vettoreAppoggio.push_back(line[i]);
             numeroBus = numeroBus + line[i];
        }

        _buses[b].FuelCost = stoi(numeroBus);
      }

      //chiudo il file.
      file.close();

    }
    catch (exception) {
      _busesNumber = 0;
      _buses.clear();

      throw runtime_error("Something goes wrong");
    }

}

const Bus &BusStation::GetBus(const int &idBus) const {
    if(idBus >= _busesNumber)
    {
        throw runtime_error("Bus " + to_string(idBus) + " does not exists");
    }
    return _buses[idBus - 1];
}

void MapData::Reset()
  {
    _routesNumber = 0;
    _streetsNumber = 0;
    _busesStopsNumber = 0;
    _busesStops.clear();
    _streets.clear();
    _routes.clear();
    _streetsFrom.clear();
    _streetsTo.clear();
    _routeStreets.clear();
  }

void MapData::Load() {
    /// Reset map
    Reset();

    //apro il file
    ifstream file;
    file.open(_mapFilePath.c_str());

    if (file.fail())
      throw runtime_error("Something goes wrong");

    //carico
    try {
      string line;

      getline(file, line); //salto il commento
      getline(file, line);
      _busesStopsNumber = stoi(line);

      getline(file, line); //salto il commento
      _busesStops.reserve(_busesStopsNumber);
      for (int b = 0; b < _busesStopsNumber; b++)
      {
        _busesStops.push_back(BusStop());
        getline(file, line);
        istringstream converter;
        converter.str(line);
        converter >> _busesStops[b].Id >> _busesStops[b].Name >> _busesStops[b].Latitude >> _busesStops[b].Longitude;
      }

      //prendo le strade
      getline(file, line); //salto il commento
      getline(file, line);
      _streetsNumber = stoi(line);

      getline(file, line); //salto il commento
      _streets.reserve(_streetsNumber);
      _streetsFrom.reserve(_streetsNumber);
      _streetsTo.reserve(_streetsNumber);
      for (int s = 0; s < _streetsNumber; s++)
      {
        _streets.push_back(Street());
        _streetsFrom.push_back(0);
        _streetsTo.push_back(0);
        getline(file, line);
        istringstream converter;
        converter.str(line);
        converter >> _streets[s].Id >> _streetsFrom[s] >> _streetsTo[s] >> _streets[s].TravelTime;
      }

      ///prendo i percorsi
      getline(file, line); //salto il commento
      getline(file, line);
      _routesNumber = stoi(line);

      getline(file, line); //salto il commento
      _routes.resize(_routesNumber);
      _routeStreets.resize(_routesNumber);
      for (int r = 0; r < _routesNumber; r++)
      {
        getline(file, line);
        istringstream converter;
        converter.str(line);
        converter >> _routes[r].Id >> _routes[r].NumberStreets;
        _routeStreets[r].resize(_routes[r].NumberStreets);

        for (int s = 0; s < _routes[r].NumberStreets; s++)
          converter >> _routeStreets[r][s];
      }
      //chiudo il file
      file.close();
    } catch (exception) {
      Reset();

      throw runtime_error("Something goes wrong");
    }
}

const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const {
    if (idRoute > _routesNumber)
      throw runtime_error("Route " + to_string(idRoute) + " does not exists");

    const Route& route = _routes[idRoute - 1];

    if (streetPosition >= route.NumberStreets)
      throw runtime_error("Street at position " + to_string(streetPosition) + " does not exists");

    int idStreet = _routeStreets[idRoute - 1][streetPosition];

    return _streets[idStreet - 1];
}

const Route &MapData::GetRoute(const int &idRoute) const {
    if (idRoute > _routesNumber)
    {
      throw runtime_error("Route " + to_string(idRoute) + " does not exists");
    }

    return _routes[idRoute - 1];
}

const Street &MapData::GetStreet(const int &idStreet) const {
    if (idStreet > _streetsNumber)
      throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    return _streets[idStreet - 1];
}

const BusStop &MapData::GetStreetFrom(const int &idStreet) const {

    if (idStreet > _streetsNumber)
      throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    int idFrom = _streetsFrom[idStreet - 1];

    return _busesStops[idFrom - 1];
}

const BusStop &MapData::GetStreetTo(const int &idStreet) const {
    if (idStreet > _streetsNumber)
      throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    int idTo = _streetsTo[idStreet - 1];

    return _busesStops[idTo - 1];
}

const BusStop &MapData::GetBusStop(const int &idBusStop) const {

    //controllo che esista tale fermata
    if(idBusStop >  _busesStopsNumber)
    {
        throw runtime_error("BusStop " + to_string(idBusStop) + " does not exists");
    }

    return _busesStops[idBusStop - 1];
}

int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const {

    Route routeSelezionata = _mapData.GetRoute(idRoute);

    int tempoViaggio = 0;
    for (int i = 0; i < routeSelezionata.NumberStreets; i++)
    {
        tempoViaggio = tempoViaggio + _mapData.GetRouteStreet(idRoute, i).TravelTime;
    }
    return tempoViaggio;
}

int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const {

    double costoTotaleViaggio = 0;
    int tempoPercorrenza = 0;
    double distanzaPercorsa = 0;

    ///converto la velocita da km/h a m/s.
    double velocitaBus = BusAverageSpeed/(3.6);

    Route routeSelezionata = _mapData.GetRoute(idRoute);
    Bus busSelezionato = _busStation.GetBus(idBus);


    for(int i = 0; i < routeSelezionata.NumberStreets; i++)
    {
        tempoPercorrenza = (_mapData.GetRouteStreet(idRoute,i).TravelTime);
        distanzaPercorsa = velocitaBus*tempoPercorrenza;

        //converto la distanza da metri a chilometri
        distanzaPercorsa = (distanzaPercorsa/1000);

        //calcolo il costo totale
        costoTotaleViaggio = costoTotaleViaggio + (distanzaPercorsa)*busSelezionato.FuelCost;
    }
    return int(costoTotaleViaggio);
}

string MapViewer::ViewRoute(const int &idRoute) const {
    const Route& route = _mapData.GetRoute(idRoute);

    int i = 0;
    ostringstream routeView;
    routeView<< to_string(idRoute)<< ": ";
    for (; i < route.NumberStreets - 1; i++)
    {
      int idStreet = _mapData.GetRouteStreet(idRoute, i).Id;
      string from = _mapData.GetStreetFrom(idStreet).Name;
      routeView << from<< " -> ";
    }

    int idStreet = _mapData.GetRouteStreet(idRoute, i).Id;
    string from = _mapData.GetStreetFrom(idStreet).Name;
    string to = _mapData.GetStreetTo(idStreet).Name;
    routeView << from<< " -> "<< to;

    return routeView.str();
}

string MapViewer::ViewStreet(const int &idStreet) const {
    const BusStop& from = _mapData.GetStreetFrom(idStreet);
    const BusStop& to = _mapData.GetStreetTo(idStreet);

    return to_string(idStreet) + ": " + from.Name + " -> " + to.Name;
}

string MapViewer::ViewBusStop(const int &idBusStop) const {
    const BusStop& busStop = _mapData.GetBusStop(idBusStop);

    return busStop.Name + " (" + to_string((double)busStop.Latitude / 10000.0) + ", " + to_string((double)busStop.Longitude / 10000.0) + ")";
}

}

