import sys

# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text
def importText(inputFilePath):

    file = open(inputFilePath, 'r')
    text = file.readlines()
    file.close()
    return True, text

# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text
def encrypt(text, password):
    j = 0
    i = 0
    lenText = len(text[0])
    encryptedText = ""
    lenPsw = len(password)
    for i in range (0, lenText):
        if (j == lenPsw):
            j = 0

        #converto i caratteri in interi, e li sommo
        temp = ord(text[0][i]) + ord(password[j])
        #converto ora l'intero in carattere
        temp = chr(temp)
        encryptedText = encryptedText + temp
        j = j + 1

    return True, encryptedText

# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text
def decrypt(text, password):
    j = 0
    i = 0
    lenText = len(text)
    lenPsw = len(password)
    decryptedTextString = ""
    for i in range(0, lenText):
        if (j == lenPsw):
            j = 0
        # converto i caratteri in interi, e ne calcolo la differenza.
        temp = ord(text[i]) - ord(password[j])
        # converto ora l'intero in carattere
        temp = chr(temp)
        decryptedTextString = decryptedTextString + temp
        j = j + 1
    #creo una lista e appendo il testo decrittato.
    decryptedText = []
    decryptedText.append(decryptedTextString)
    return True,decryptedText


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Password shall passed to the program")
        exit(-1)
    password = sys.argv[1]

    inputFileName = "text.txt"

    [resultImport, text] = importText(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text=", text)

    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result= ", encryptedText)

    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result= ", decryptedText)
