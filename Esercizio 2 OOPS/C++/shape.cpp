#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Point::Point(const double &x, const double &y)
{

    _x = x; //the definition of the constructor
    _y = y;
}

Ellipse::Ellipse(const Point &center, const int &a, const int &b)
/* avendo definito il costruttore vuoto nella classe point,
non c'è bisogno di chiamarlo prima del costruttore di Ellipse,
tuttavia nel codice successivo ho preferito chiamarlo prima
del costruttore delle altri classi, per avere ben chiare le due prospettive*/
{
    _center = center;
    _a = a;
    _b = b;

}
double Ellipse::Area() const
{
    return _a*_b*M_PI;

}

Circle::Circle(const Point &center, const int &radius) : Ellipse(center, radius, radius),  _center(center)
{

}

double Circle::Area() const
{
    return Ellipse::Area();
}

Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3) : _p1(p1), _p2(p2), _p3(p3)
{
    _p1 = p1;
    _p2 = p2;
    _p3 = p3;
}

double Triangle::Area() const
{
    return (0.5*abs(_p1._x*_p2._y + _p2._x*_p3._y +  _p3._x*_p1._y - _p2._x*_p1._y - _p3._x*_p2._y - _p1._x*_p3._y));
}
TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge) : Triangle(p1 ,Point(p1._x + edge, p1._y), Point(p1._x + edge/2, p1._y + edge*sqrt(3)/2)), _p1(p1)
{
    _p1 = p1;
    _edge = edge;

}

double TriangleEquilateral::Area() const
{
    return Triangle::Area();
}

Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4) : _p1(p1), _p2(p2), _p3(p3), _p4(p4)
{
    _p1 = p1;
    _p2 = p2;
    _p3 = p3;
    _p4 = p4;
}

double Quadrilateral::Area() const
{

    return (0.5*abs(_p1._x*_p2._y + _p2._x*_p3._y +  _p3._x*_p4._y + _p4._x*_p1._y - _p2._x*_p1._y - _p3._x*_p2._y - _p4._x*_p3._y - _p1._x*_p4._y));
}

Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4) : Quadrilateral(p1, p2, Point (p4._x + abs(p2._x - p1._x), p4._y), p4), _p1(p1), _p2(p2), _p4(p4)
{
    _p1 = p1;
    _p2 = p2;
    _p4 = p4;
}

double Parallelogram::Area() const
{
    return Quadrilateral::Area();
}

Rectangle::Rectangle(const Point &p1, const int &base, const int &height) : Parallelogram(p1, Point(p1._x + base, p1._y), Point(p1._x, p1._y + height)), _p1(p1)
{
    _p1 = p1;
    _base = base;
    _height = height;
}

double Rectangle::Area() const
{
    return Parallelogram::Area();
}

Square::Square(const Point &p1, const int &edge) : Rectangle(p1, edge, edge), _p1(p1)
{
    _p1 = p1;
    _edge = edge;
}

double Square::Area() const
{
    return Rectangle::Area();
}





















}
