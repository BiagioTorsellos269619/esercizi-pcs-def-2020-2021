import math

class Point:
    def __init__(self, x: float, y: float):
        self._x = x
        self._y = y
    def Point(self, point):
        self._x = point._x
        self._y = point._y


class IPolygon:
    def area(self) -> float:
        return 0.0


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self._center = center
        self._a = a
        self._b = b

    def area(self):
        return self._a*self._b*(math.pi)


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        self._center = center
        self._radius = radius
        Ellipse.__init__(self, center, radius, radius) #costruttore della classe madre

    def area(self):
        return Ellipse.area(self)


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3
    def area(self):
        return (0.5 * abs(self._p1._x * self._p2._y + self._p2._x * self._p3._y + self._p3._x * self._p1._y - self._p2._x * self._p1._y - self._p3._x * self._p2._y - self._p1._x *self._p3._y))


class TriangleEquilateral(Triangle):
    def __init__(self, p1: Point, edge: int):
        self._p1 = p1
        self._edge = edge
        Triangle.__init__(self, p1 , Point(p1._x + edge, p1._y), Point(p1._x + edge/2, p1._y + edge*pow(3, 1/2)/2)) #costruttore della classe madre
    def area(self):
        return Triangle.area(self)

class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3
        self._p4 = p4
    def area(self):
        return (0.5 * abs(self._p1._x * self._p2._y + self._p2._x *self._p3._y + self._p3._x * self._p4._y + self._p4._x * self._p1._y - self._p2._x * self._p1._y - self._p3._x *self._p2._y - self._p4._x * self._p3._y - self._p1._x * self._p4._y))


class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        self._p1 = p1
        self._p2 = p2
        self._p4 = p4
        Quadrilateral.__init__(self, p1, p2, Point (p4._x + abs(p2._x - p1._x), p4._y), p4) #costruttore della classe madre
    def area(self):
        return Quadrilateral.area(self)

class Rectangle(Parallelogram):
    def __init__(self, p1: Point, base: int, height: int):
        self._p1 = p1
        self._base = base
        self._height = height
        Parallelogram.__init__(self,p1, Point(p1._x + base, p1._y), Point(p1._x, p1._y + height)) #costruttore della classe madre
    def area(self):
        return Parallelogram.area(self)

class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        self._p1 = p1
        self._edge = edge
        Rectangle.__init__(self, p1, edge, edge) #costruttore della classe madre
    def area(self):
        return Rectangle.area(self)