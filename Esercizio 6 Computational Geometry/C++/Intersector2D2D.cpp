#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    toleranceParallelism = 1E-05;
    toleranceIntersection = 1E-07;
    intersectionType = Intersector2D2D::NoInteresection;
}


Intersector2D2D::~Intersector2D2D()
{

}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.col(0) = planeNormal;
    rightHandSide[0] = planeTranslation;
    return;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.col(1) = planeNormal;
    rightHandSide[1] = planeTranslation;
    return;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    //calcolo il prodotto vettoriale di n1 e n2 per trovare la normale n3,
    //ed assegnarla alla terza colonna della matrice.
    Matrix3d matrixProduct;
    bool normalParalleli = true;

    matrixProduct.row(1) = matrixNomalVector.col(0);
    matrixProduct.row(2) = matrixNomalVector.col(1);
    matrixProduct.row(0) = Vector3d(1,1,1);

    tangentLine[0] = (matrixProduct(1,1)*matrixProduct(2,2)) - (matrixProduct(1,2)*matrixProduct(2,1));
    tangentLine[1] = -((matrixProduct(1,0)*matrixProduct(2,2)) - (matrixProduct(1,2)*matrixProduct(2,0)));
    tangentLine[2] = (matrixProduct(1,0)*matrixProduct(2,1)) - (matrixProduct(2,0)*matrixProduct(1,1));

    //verifico se n1 e n2 sono paralleli, ovvero se n3 = 0 (vettore nullo, a meno di una tolleranza)
    for(unsigned int i = 0; i < 3 && normalParalleli == true; i++)
    {
        if(tangentLine[i] > -toleranceParallelism && tangentLine[i] < toleranceParallelism)
        {
            normalParalleli = true;
        }
        else
            normalParalleli = false;
    }
    if(normalParalleli == true) //n1 e n2 sono paralleli
    {
        if(abs(rightHandSide[0] - rightHandSide[1]) < toleranceIntersection)
        {
             //i due piani sono coincidenti
             intersectionType = Intersector2D2D::Coplanar;
             return true;
        }
        else // i due piani sono distinti
        {
             intersectionType = Intersector2D2D::NoInteresection;
             return false;
        }
    }

    ///Se arriviamo a questo punto, allora i due piani si intersecano

    //scelgo il terzo piano passante per l'origine.

    rightHandSide[2] = 0;

    //trovo il punto che risolve il sistema.

    pointLine = matrixProduct.fullPivLu().solve(rightHandSide);

    //la retta d'intersezione è data a questo punto da pointLine + s*n3, dove n3 = tangentLine.

    intersectionType = Intersector2D2D::LineIntersection;
    return true;
}
